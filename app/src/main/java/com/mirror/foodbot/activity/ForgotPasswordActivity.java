package com.mirror.foodbot.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mirror.foodbot.R;
import com.mirror.foodbot.common.MessageConstant;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordActivity extends Activity implements OnClickListener {

    EditText memail_address;
    Button msubmit;
    TextView mreset;

    ProgressDialog progressDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);

        memail_address = (EditText) findViewById(R.id.editTextemailaddress);
        msubmit = (Button) findViewById(R.id.button_submit);
        mreset = (TextView) findViewById(R.id.textview_reset_link);


        msubmit.setOnClickListener(this);
        mreset.setOnClickListener(this);

    }

    public void onClick(View v) {
        // TODO Auto-generated method stub

        switch (v.getId()) {
            case R.id.textview_reset_link:
                Intent i = new Intent(this, ResetPasswordActivity.class);
                startActivity(i);
                break;

            case R.id.button_submit:
                RequestQueue queue = Volley.newRequestQueue(this);
                String email_address = memail_address.getText().toString();
                JSONObject req = new JSONObject();
                try {
                    req.put("emailAddress", email_address);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                String url = MessageConstant.FORGOT_PASSWORD_URL;
                JsonObjectRequest jsonobj = new JsonObjectRequest(Request.Method.POST, url, req,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("Chatbot", "Reset code will be sent to your Email address ");
                                System.out.println("Response: " + response);
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Reset code will be sent to your Email address if email address is valid user", Toast.LENGTH_SHORT).show();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Error while connecting", Toast.LENGTH_SHORT).show();
                            }
                        }
                ) {
                    //here I want to post data to sever
                };

                int socketTimeout = 10000;//2 minutes - change to what you want
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                jsonobj.setRetryPolicy(policy);
                queue.add(jsonobj);
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Validating User....");
                progressDialog.show();
        }
    }


}
