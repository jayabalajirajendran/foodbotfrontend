package com.mirror.foodbot.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mirror.foodbot.R;
import com.mirror.foodbot.common.MessageConstant;


import org.json.JSONException;
import org.json.JSONObject;

public class HomeActivity extends Activity implements OnClickListener {

    EditText memail_address;
    EditText m_password;
    Button mlogin;
    Button mregister;
    TextView mforgot;

    ProgressDialog progressDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        memail_address = (EditText) findViewById(R.id.editTextemailaddress);
        m_password = (EditText) findViewById(R.id.editTextPassword);
        mlogin = (Button) findViewById(R.id.button_login);
        mregister = (Button) findViewById(R.id.button_register);
        mforgot = (TextView) findViewById(R.id.textview_forgot_link);


        mlogin.setOnClickListener(this);
        mregister.setOnClickListener(this);
        mforgot.setOnClickListener(this);


    }

    public void onClick(View v) {
        // TODO Auto-generated method stub

        switch (v.getId()) {
            case R.id.button_register:
                Intent i = new Intent(this, RegisterActivity.class);
                startActivity(i);
                break;
            case R.id.textview_forgot_link:
                Intent j = new Intent(this, ForgotPasswordActivity.class);
                startActivity(j);
                break;
            case R.id.button_login:
                RequestQueue queue = Volley.newRequestQueue(this);
                String email_address = memail_address.getText().toString();
                String password = m_password.getText().toString();
                JSONObject req = new JSONObject();
                try {
                    MainActivity.req.put("emailAddress", email_address);
                    req.put("emailAddress", email_address);
                    req.put("password", password);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                String url = MessageConstant.LOGIN_URL;
                JsonObjectRequest jsonobj = new JsonObjectRequest(Request.Method.POST, url, req,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                Log.d("Chatbot", "Success response");
                                System.out.println("Response: " + response);
                                progressDialog.dismiss();

                                try {
                                    if ((response.getString("response")).equals("Success")) {
                                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(i);
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Invalid User Name / Password", Toast.LENGTH_SHORT).show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Error while connecting", Toast.LENGTH_SHORT).show();

                            }
                        }
                ) {
                    //here I want to post data to sever
                };
                // Add the request to the RequestQueue.
                int socketTimeout = 10000;//2 minutes - change to what you want
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                jsonobj.setRetryPolicy(policy);
                queue.add(jsonobj);
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Authenticating User....");
                progressDialog.show();
        }

    }

}
