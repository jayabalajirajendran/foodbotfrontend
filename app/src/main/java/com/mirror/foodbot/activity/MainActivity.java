package com.mirror.foodbot.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.mirror.foodbot.common.MessageConstant;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mirror.foodbot.adapter.ChatArrayAdapter;
import com.mirror.foodbot.model.ChatMessage;
import com.mirror.foodbot.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends Activity {
    private static final String TAG = "ChatActivity";
    private ChatArrayAdapter adp;
    private ListView list;
    private EditText chatText;
    private Button send;
    ProgressDialog progressDialog;
    public static int flow = 1;
    public static String type = "";
    TextToSpeech t1;
    public static JSONObject req = new JSONObject();
    private boolean side = true;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        setContentView(R.layout.main);
        send = (Button) findViewById(R.id.btn);
        list = (ListView) findViewById(R.id.listview);
        flow = 1;
        adp = new ChatArrayAdapter(getApplicationContext(), R.layout.chat);
        list.setAdapter(adp);
        chatText = (EditText) findViewById(R.id.chat_text);
        chatText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode ==
                        KeyEvent.KEYCODE_ENTER)) {
                    return sendChatMessage();
                }
                return false;
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                sendChatMessage();
            }
        });
        list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        list.setAdapter(adp);
        adp.add(new ChatMessage(side, "chat", "Hi, Welcome to FoodBot \n Thank you so much for stepping in! \n May I know your name please?", 0));
        side = !side;
    }

    public void onPause() {
        super.onPause();
    }

    private boolean sendChatMessage() {
        RequestQueue queue = Volley.newRequestQueue(this);
        adp.add(new ChatMessage(side, "chat", chatText.getText().toString()));
        side = !side;
        String url = "";
        String bodyText = chatText.getText().toString();
        List<ChatMessage> previousChatList = new ArrayList<ChatMessage>();
        try {
            if (bodyText.contains("?") || bodyText.contains("!")) {
                url = MessageConstant.FAQ_URL;
                req.put("question", chatText.getText().toString());
                req.put("flow", flow);
                for (ChatMessage chatMessage : adp.getMessageList()) {
                    if (chatMessage.type.equalsIgnoreCase("chat")) {
                        previousChatList.add(chatMessage);
                    }
                }
                if (previousChatList.size() == 1) {
                    req.put("previousChat", "May I know your name please");
                } else {
                    req.put("previousChat", previousChatList.get(previousChatList.size() - 2).message);
                }
                type = "FAQ";
            } else if (flow == 6) {
                url = MessageConstant.SUCCESS_URL;
                req.put("phoneNumber", chatText.getText().toString());
                String regex = "\\d{10}";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(chatText.getText().toString());
                if (matcher.matches()) {
                    adp.add(new ChatMessage(side, "chat", "Thank you so much for your kind help. " +
                            "\n Please spread the importance of food waste. \n An Email will be sent to you for the reference", 6));
                    adp.add(new ChatMessage(side, "chat", "Is there anything else you wish to ask?"));
                    side = !side;
                } else {
                    adp.add(new ChatMessage(side, "chat", "Please enter a valid phone number \n to contact you for collection.", 5));
                    flow = 6;
                    side = !side;
                    return true;
                }
            } else if (flow == 5) {
                url = MessageConstant.SAVE_URL;
                req.put("expiryDate", chatText.getText().toString());
                String regex = "^[0-3]?[0-9]/[0-3]?[0-9]/(?:[0-9]{2})?[0-9]{2}$";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(chatText.getText().toString());
                if (matcher.matches() && checkDate(req.getString("expiryDate"))) {
                    adp.add(new ChatMessage(side, "chat", "Perfect! As a final step, Please enter \n your phone number to contact for collection.", 5));
                    flow = 6;
                    side = !side;
                } else {
                    adp.add(new ChatMessage(side, "chat", "Oops, Its invalid.! Please enter a valid expiry date of the " + req.get("donateItem") + " in format DD/MM/YYYY", 4));
                    flow = 5;
                    side = !side;
                    return true;
                }
            } else if (flow == 4) {
                url = MessageConstant.SAVE_URL;
                req.put("location", chatText.getText().toString());
                adp.add(new ChatMessage(side, "chat", "Can you please enter the expiry date of the " + req.get("donateItem") + " in format DD/MM/YYYY", 4));
                flow = 5;
                side = !side;
            } else {
                url = MessageConstant.NAME_URL;
                req.put("doc", chatText.getText().toString());
                req.put("name", chatText.getText().toString());
            }
        } catch (JSONException | ParseException e) {
            // TODAuto-generated catch block
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, req,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("ChatBot", "Success response");
                        System.out.println("Response: " + response);
                        try {
                            String data = response.getString("resp");
                            if (flow == 1 && !type.toLowerCase().equalsIgnoreCase("FAQ")) {
                                adp.add(new ChatMessage(side, "chat", data, flow));
                                flow = 2;
                                side = !side;
                            } else if (type.toLowerCase().equalsIgnoreCase("FAQ")) {
                                flow = (int) req.get("flow");
                                adp.add(new ChatMessage(side, "FAQ", data));
                                if (flow != 6) {
                                    adp.add(new ChatMessage(side, "FAQ", "Lets continue the chat from where we left."));
                                    adp.add(new ChatMessage(side, "FAQ", req.get("previousChat").toString(), flow));
                                }
                                type = "";
                                side = !side;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Error while connecting to service", Toast.LENGTH_LONG).show();
                        }

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Error while connecting to service", Toast.LENGTH_SHORT).show();

                    }
                }
        ) {
            //here I want to post data to sever
        };

        // Add the request to the RequestQueue.
        int socketTimeout = 10000;//2 minutes - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        jsonObjectRequest.setRetryPolicy(policy);
        queue.add(jsonObjectRequest);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Data....");
        progressDialog.show();
        return true;
    }

    public Boolean checkDate(String date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date currentDate = sdf.parse(sdf.format(new Date()));
        System.out.println(currentDate);
        Date enteredDate = sdf.parse(date);
        System.out.println(enteredDate);
        return currentDate != null && (currentDate.equals(enteredDate) || currentDate.before(enteredDate));
    }
}

