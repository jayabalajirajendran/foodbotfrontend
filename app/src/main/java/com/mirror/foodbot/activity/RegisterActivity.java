package com.mirror.foodbot.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.mirror.foodbot.common.MessageConstant;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mirror.foodbot.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterActivity extends Activity implements OnClickListener {

    EditText memail_address;
    EditText mfirst_name;
    EditText mlast_name;
    EditText m_password;
    EditText m_confirm_password;
    EditText mdob;
    RadioButton mradio_male;
    RadioButton mradio_female;
    Button mregister;
    ProgressDialog progressDialog;
    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);


         date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
// TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }



        };
        memail_address = (EditText) findViewById(R.id.editTextemailaddress);
        m_password = (EditText) findViewById(R.id.editTextPassword);
        m_confirm_password = (EditText) findViewById(R.id.editTextConfirmPassword);
        mfirst_name =(EditText) findViewById(R.id.editTextFirstName);
        mlast_name =(EditText) findViewById(R.id.editTextLastName);
        mdob =(EditText) findViewById(R.id.editTextDate);
        //mlogin = (Button) findViewById(R.id.button_login);
        mregister = (Button) findViewById(R.id.button_register);
        mradio_male = (RadioButton) findViewById(R.id.radioButtonMale);
        mradio_female = (RadioButton) findViewById(R.id.radioButtonFemale);


        //mlogin.setOnClickListener(this);
        mregister.setOnClickListener(this);
        mradio_male.setOnClickListener(this);
        mradio_female.setOnClickListener(this);
        mdob.setOnClickListener(this);

    }

    public void onClick(View v) {
        // TODO Auto-generated method stub

        switch (v.getId()){

            case R.id.editTextDate:
                DatePickerDialog datePickerDialog =new DatePickerDialog(this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
                break;

            case R.id.button_register:

                RequestQueue queue = Volley.newRequestQueue(this);

                String email_address = memail_address.getText().toString();
                String first_name = mfirst_name.getText().toString();
                String last_name = mlast_name.getText().toString();
                String password = m_password.getText().toString();
                String con_password = m_confirm_password.getText().toString();
                String dob = mdob.getText().toString();
                String gender = "";
                if(mradio_male.isChecked())
                    gender = "M";
                if(mradio_female.isChecked())
                    gender = "F";



                boolean valid = validation();
                if(!valid){
                    break;
                }

                JSONObject req = new JSONObject();
                try {
                    req.put("emailAddress", email_address);
                    req.put("firstName", first_name);
                    req.put("lastName", last_name);
                    req.put("dateOfBirth", dob);
                    req.put("gender", gender);
                    req.put("password", password);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                String url =MessageConstant.REGISTER_URL;

                JsonObjectRequest jsonobj = new JsonObjectRequest(Request.Method.POST, url,req,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                Log.d("Chatbot","Success response");
                                System.out.println("Response: " + response);
                                progressDialog.dismiss();

                                try {
                                    if((response.getString("response")).equals("success")){
                                        Toast.makeText(getApplicationContext(),"User ID created successfully",Toast.LENGTH_SHORT).show();
                                        //((Activity)getApplicationContext()).finish();
                                        Intent i = new Intent(getApplicationContext(),HomeActivity.class);
                                        startActivity(i);

                                    }else{
                                        Toast.makeText(getApplicationContext(),"Error while creating user profile",Toast.LENGTH_SHORT).show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }



                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(),"Error while connecting",Toast.LENGTH_SHORT).show();

                            }
                        }
                ){
                    //here I want to post data to sever
                };

// Add the request to the RequestQueue.
                int socketTimeout = 10000;//2 minutes - change to what you want
                //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                jsonobj.setRetryPolicy(policy);
                queue.add(jsonobj);
                //initialize the progress dialog and show it
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("User Registration....");
                progressDialog.show();



                break;



        }




    }

    public boolean validation(){

        String email_address = memail_address.getText().toString();
        String first_name = mfirst_name.getText().toString();
        String last_name = mlast_name.getText().toString();
        String password = m_password.getText().toString();
        String con_password = m_confirm_password.getText().toString();
        String dob = mdob.getText().toString();

        final String EMAIL_PATTERN = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        Pattern pattern;
        Matcher matcher;

        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email_address);
        if(!matcher.matches()){
            Toast.makeText(this,"Email address not in valid format",Toast.LENGTH_LONG).show();
            return false;

        }

        if (first_name.trim().length() == 0){
            Toast.makeText(this,"First Name cannot be empty",Toast.LENGTH_LONG).show();
            return false;
        }


        if (last_name.trim().length() == 0){
            Toast.makeText(this,"Last Name cannot be empty",Toast.LENGTH_LONG).show();
            return false;
        }




        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        if(!matcher.matches()){
            Toast.makeText(this,"Password should contains a minimum of 6 characters with at least one capital letter,one number and one special character",Toast.LENGTH_LONG).show();
            return false;

        }

        if(!password.equals(con_password)){
            Toast.makeText(this,"Password and confirm password does not match",Toast.LENGTH_LONG).show();
            return false;


        }

        return true;
    }

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        mdob.setText(sdf.format(myCalendar.getTime()));
    }

}
