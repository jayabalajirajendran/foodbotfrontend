package com.mirror.foodbot.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mirror.foodbot.R;
import com.mirror.foodbot.common.MessageConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ResetPasswordActivity extends Activity implements OnClickListener {

    EditText memail_address;
    EditText m_resetcode;

    EditText mpassword;
    EditText mconfirm_password;

    Button mreset;
    Button mreset_submit;

    LinearLayout reset_code_layout;
    LinearLayout reset_password_layout;

    ProgressDialog progressDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reset_password);

        memail_address = (EditText) findViewById(R.id.editTextemailaddress);
        m_resetcode = (EditText) findViewById(R.id.editTextresetCode);
        mreset = (Button) findViewById(R.id.button_reset);
        mreset_submit = (Button) findViewById(R.id.button_reset_submit);

        mpassword = (EditText) findViewById(R.id.editTextresetPass);
        mconfirm_password = (EditText) findViewById(R.id.editTextresetconfirmPass);

        reset_code_layout = (LinearLayout) findViewById(R.id.reset_code_layout);
        reset_password_layout = (LinearLayout) findViewById(R.id.reset_password_layout);


        mreset.setOnClickListener(this);
        mreset_submit.setOnClickListener(this);

    }

    public void onClick(View v) {
        // TODO Auto-generated method stub

        switch (v.getId()) {
            case R.id.button_reset_submit:
                if (!validation()) {
                    break;
                }
                RequestQueue queue1 = Volley.newRequestQueue(this);
                String password = mpassword.getText().toString();
                String con_password = mconfirm_password.getText().toString();
                String email_address1 = memail_address.getText().toString();
                JSONObject req1 = new JSONObject();
                try {
                    req1.put("emailAddress", email_address1);
                    req1.put("password", password);
                    req1.put("con_password", con_password);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                String url1 = MessageConstant.VERIFY_URL;

                JsonObjectRequest jsonobj1 = new JsonObjectRequest(Request.Method.POST, url1, req1,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                Log.d("Chatbot", "Success response");
                                System.out.println("Response: " + response);
                                progressDialog.dismiss();

                                try {
                                    if ((response.getString("response")).equals("success")) {
                                        Toast.makeText(getApplicationContext(), "Password reset successful", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getApplicationContext(), HomeActivity.class);        // Specify any activity here e.g. home or splash or login etc
                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        i.putExtra("EXIT", true);
                                        startActivity(i);
                                        finish();

                                    } else {
                                        Toast.makeText(getApplicationContext(), "Error while password reset", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Error while connecting", Toast.LENGTH_SHORT).show();

                            }
                        }
                ) {
                    //here I want to post data to sever
                };

                int socketTimeout = 10000;
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                jsonobj1.setRetryPolicy(policy);
                queue1.add(jsonobj1);
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Validating User....");
                progressDialog.show();
                break;
            case R.id.button_reset:
                RequestQueue queue = Volley.newRequestQueue(this);
                String email_address = memail_address.getText().toString();
                final String reset_code = m_resetcode.getText().toString();
                JSONObject req = new JSONObject();
                try {
                    req.put("emailAddress", email_address);
                    req.put("resetCode", reset_code);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                String url =  MessageConstant.VERIFY_URL;
                JsonObjectRequest jsonobj = new JsonObjectRequest(Request.Method.POST, url, req,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                Log.d("Chatbot", "Success response");
                                System.out.println("Response: " + response);
                                progressDialog.dismiss();

                                try {
                                    if ((response.getString("response")).equals("Success")) {
                                        Toast.makeText(getApplicationContext(), "Code valid", Toast.LENGTH_LONG).show();
                                        reset_code_layout.setVisibility(View.GONE);
                                        reset_password_layout.setVisibility(View.VISIBLE);

                                    } else {
                                        Toast.makeText(getApplicationContext(), "Invalid email address / reset code", Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Error while connecting", Toast.LENGTH_SHORT).show();

                            }
                        }
                ) {
                    //here I want to post data to sever
                };

                int socketTimeout1 = 10000;
                RetryPolicy policy1 = new DefaultRetryPolicy(socketTimeout1, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                jsonobj.setRetryPolicy(policy1);
                queue.add(jsonobj);
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Validating User....");
                progressDialog.show();
        }
    }

    public boolean validation() {
        String password = mpassword.getText().toString();
        String con_password = mconfirm_password.getText().toString();
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        if (!matcher.matches()) {
            Toast.makeText(this, "Password not in valid format", Toast.LENGTH_LONG).show();
            return false;

        }
        if (!password.equals(con_password)) {
            Toast.makeText(this, "Password and confirm password does not match", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }


}
