package com.mirror.foodbot.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mirror.foodbot.model.ChatMessage;
import com.mirror.foodbot.R;
import com.mirror.foodbot.activity.MainActivity;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


public class ChatArrayAdapter extends ArrayAdapter<ChatMessage> {
    private TextView chatText;
    private List<ChatMessage> MessageList = new ArrayList<ChatMessage>();
    private LinearLayout layout;
    private LinearLayout chatlayout;
    private LinearLayout donationButtonlayout;
    private LinearLayout organisationButtonlayout;
    private Button mbutton_sandwich;
    private Button mbutton_pastry;
    private boolean side;
    private Button mbutton_circlek;
    private Button mbutton_tesco;

    public ChatArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public void add(ChatMessage object) {
        // TODO Auto-generated method stub

        MessageList.add(object);
        super.add(object);
    }

    public int getCount() {
        return this.MessageList.size();
    }

    public List<ChatMessage> getMessageList() {
        return MessageList;
    }

    public ChatMessage getItem(int index) {

        return this.MessageList.get(index);
    }

    public View getView(int position, View ConvertView, ViewGroup parent) {

        View v = ConvertView;
        if (v == null) {

            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.chat, parent, false);

        }

        layout = (LinearLayout) v.findViewById(R.id.Message1);
        chatlayout = (LinearLayout) v.findViewById(R.id.chat_layout);
        donationButtonlayout = (LinearLayout) v.findViewById(R.id.DonationButtons);
        organisationButtonlayout = (LinearLayout) v.findViewById(R.id.OrganisationButtons);
        mbutton_sandwich = (Button) v.findViewById(R.id.button_sandwich);
        mbutton_pastry = (Button) v.findViewById(R.id.button_pastry);
        mbutton_circlek = (Button) v.findViewById(R.id.button_circlek);
        mbutton_tesco = (Button) v.findViewById(R.id.button_tesco);
        ChatMessage messageobj = getItem(position);
        chatText = (TextView) v.findViewById(R.id.SingleMessage);
        chatText.setText(messageobj.message);
        Linkify.addLinks(chatText, Linkify.PHONE_NUMBERS | Linkify.WEB_URLS);
        chatText.setBackgroundResource(messageobj.left ? R.drawable.out : R.drawable.in);
        layout.setGravity(messageobj.left ? Gravity.LEFT : Gravity.RIGHT);
        chatlayout.setGravity(messageobj.left ? Gravity.LEFT : Gravity.RIGHT);
        Log.d("DebugChat", "position:" + position);
        Log.d("DebugChat", "message:" + messageobj.message);
        Log.d("DebugChat", "flow:" + messageobj.flow);
        donationButtonlayout.setVisibility(View.GONE);
        organisationButtonlayout.setVisibility(View.GONE);
        side = true;
        if (messageobj.flow == 1) {
            donationButtonlayout.setVisibility(View.VISIBLE);
            mbutton_sandwich.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // DO what you want to receive on btn click there.
                    try {
                        Log.d("Debug Chat", "Layout clicked");
                        add(new ChatMessage(false,"chat", mbutton_sandwich.getText().toString()));
                        add(new ChatMessage(side,"chat", "Sure, Thank you so much for \n the contribution. " +
                                "Please answer \n few more questions regarding \n your donation \n \n Please click your Organisation name?", 2));
                        MainActivity.req.put("donateItem", mbutton_sandwich.getText().toString());
                        MainActivity.flow = 2;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            mbutton_pastry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // DO what you want to receive on btn click there.
                    try {
                        Log.d("Debug Chat", "Layout clicked");
                        add(new ChatMessage(false,"chat", mbutton_pastry.getText().toString()));
                        add(new ChatMessage(side,"chat", "Sure, Thank you so much for \n the contribution. " +
                                "Please answer \n few more questions regarding \n your donation \n \n Please click your Organisation name?", 2));
                        MainActivity.req.put("donateItem", mbutton_pastry.getText().toString());
                        MainActivity.flow = 2;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        if (messageobj.flow == 2) {
            organisationButtonlayout.setVisibility(View.VISIBLE);
            mbutton_circlek.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // DO what you want to receive on btn click there.
                    try {
                        Log.d("Debug Chat", "Layout clicked");
                        add(new ChatMessage(false,"chat", mbutton_circlek.getText().toString()));
                        add(new ChatMessage(side,"chat", "Can you please enter your Circle K location?", 3));
                        MainActivity.req.put("organisation", mbutton_circlek.getText().toString());
                        MainActivity.flow = 4;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            mbutton_tesco.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // DO what you want to receive on btn click there.
                    try {
                        Log.d("Debug Chat", "Layout clicked");
                        add(new ChatMessage(false,"chat", mbutton_tesco.getText().toString()));
                        add(new ChatMessage(side,"chat", "Can you please enter your Tesco location?", 3));
                        MainActivity.req.put("quantity", mbutton_tesco.getText().toString());
                        MainActivity.flow = 4;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        return v;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }


}

