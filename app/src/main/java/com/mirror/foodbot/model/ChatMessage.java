package com.mirror.foodbot.model;


public class ChatMessage {
    public boolean left;
    public String message;
    public String type;
    public int flow;

    public ChatMessage(boolean left, String type, String message) {
        // TODO Auto-generated constructor stub
        this(left, type, message, 0);
    }

    public ChatMessage(boolean left, String type, String message, int flow) {
        // TODO Auto-generated constructor stub
        super();
        this.left = left;
        this.type = type;
        this.message = message;
        this.flow = flow;
    }
}