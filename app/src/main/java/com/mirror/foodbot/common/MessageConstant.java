package com.mirror.foodbot.common;

public class MessageConstant {
    public static final String URL = "http://ec2-54-155-153-73.eu-west-1.compute.amazonaws.com:8080";
    public static final String LOGIN_URL = URL + "/login";
    public static final String FORGOT_PASSWORD_URL = URL + "/forgotPass";
    public static final String FAQ_URL = URL + "/getFAQ";
    public static final String SUCCESS_URL = URL + "/success";
    public static final String SAVE_URL = URL + "/save";
    public static final String NAME_URL = URL + "/getName";
    public static final String REGISTER_URL = URL + "/registerUser";
    public static final String VERIFY_URL = URL + "/verify";

}
